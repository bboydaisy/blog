<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use DateTime;
use App\Models\Post;
use App\Models\User;

class CommentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'content' => $this->faker->text,
            'user_id' => User::all()->random()->id,
            'post_id' => Post::all()->random()->id,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ];
    }
}
