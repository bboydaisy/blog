<?php

namespace Database\Factories;

use App\Models\Comment;
use Illuminate\Database\Eloquent\Factories\Factory;
use DateTime;
use App\Models\User;

class ReplyCommentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'content' => $this->faker->text,
            'user_id' => User::all()->random()->id,
            'comment_id' => Comment::all()->random()->id,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ];
    }
}
