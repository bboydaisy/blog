<?php

namespace Database\Seeders;

use App\Models\ReplyComment;
use Illuminate\Database\Seeder;

class ReplyCommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ReplyComment::factory()->count(25)->create();
    }
}
