@extends('layout.main')

@section('content')
    <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
            <div class="col-lg-4 mx-auto">
                <div class="auth-form-light text-left py-5 px-4 px-sm-5">
                    <h5 class="font-weight-light">Profile <i class="icon-head"></i></h5>
                    @if (Session::has('success'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <strong>{{ Session::get('success') }}</strong>
                            <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        </div>
                    @endif
                    @if (Session::has('error'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <strong>{{ Session::get('error') }}</strong>
                            <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        </div>
                    @endif
                    <form class="pt-3" action="" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <input type="text" class="form-control form-control-lg" id="exampleInputUsername1"
                                placeholder="Username" name="name" value="{{ $user->name }}">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control form-control-lg" id="exampleInputEmail1"
                                placeholder="Email" name="email" value="{{ $user->email }}" disabled>
                        </div>
                        @if ($user->confirm_flag == 1)
                            <div class="form-group">
                                <input type="password" class="form-control form-control-lg" id="exampleInputPassword1"
                                    placeholder="Old password" name="old_password">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control form-control-lg" id="exampleInputPassword1"
                                    placeholder="New password" name="password">
                            </div>
                        @endif
                        <a class="btn ms-2 btn-danger" type="button" data-toggle="modal" data-target="#modalDisable">
                            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor"
                                class="bi bi-person-x-fill" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                    d="M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6.146-2.854a.5.5 0 0 1 .708 0L14 6.293l1.146-1.147a.5.5 0 0 1 .708.708L14.707 7l1.147 1.146a.5.5 0 0 1-.708.708L14 7.707l-1.146 1.147a.5.5 0 0 1-.708-.708L13.293 7l-1.147-1.146a.5.5 0 0 1 0-.708z" />
                            </svg> account
                        </a>
                        <div class="mt-3">
                            <button class="btn btn-block btn-info btn-lg font-weight-medium auth-form-btn">
                                Update profile
                            </button>
                        </div>
                    </form>
                    <div class="text-center mt-4 font-weight-light">
                        Back to home page? <a href="{{ url('/') }}" class="text-primary">News</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- modalDisable --}}
    @if ($user->confirm_flag == 1)
        <div class="modal fade" id="modalDisable" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="{{ url('/user') }}" method="POST" style="overflow-y: auto">
                        @csrf
                        @method('DELETE')
                        <div class="modal-body">
                            <p id="content">
                            <h6> Are you sure you want to disable this email account? </h6>
                            </p>
                            <div>
                                <label for="title-create" class="col-form-label"><b>Current password:
                                        <span style="color: red">*</span></b>
                                </label>
                                <input type="password" class="form-control" id="exampleInputPassword1"
                                    placeholder="Password" name="password" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <div>
                                <button type="submit" class="btn btn-primary">Agree</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @else
        <div class="modal fade" id="modalDisable" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="{{ url('/user') }}" method="POST" style="overflow-y: auto">
                        @csrf
                        @method('DELETE')
                        <div class="modal-body">
                            <p id="content">
                            <h6> Are you sure you want to disable this email account? </h6>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <div>
                                <button type="submit" class="btn btn-primary">Agree</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
@endsection
