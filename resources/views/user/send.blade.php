<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
    <div style="text-align: center; box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.4);">
        <p style="text-align: center;">
            <span style="font-size: 24px;">welcome to my website, {{ $email_data['name'] }}</span>
        </p>
        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSS150td34_IoLvT6U8hj-QPcUVUjHnOCj4qDDrnauJVR8U4Uqjtm2XLavcLOQb8eXGOI0&usqp=CAU"
            alt="" style="width: 150px; display: block; margin-left: auto; margin-right: auto;">
        <p style="text-align: center;">
            <span style="font-size: 16px;">
                Here are your 6 security numbers to enter to confirm. The code is only valid for 3 minutes!
            </span>
            <br>
            <p><span style="font-weight: bold; font-size: 20px;">{{ $email_data['verification_code'] }}</span></p>
        </p>
    </div>
</body>

</html>
