<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>login</title>
    <script src="{{ asset('vendors/js/vendor.bundle.base.js') }}"></script>
    <script src="{{ asset('js/off-canvas.js') }}"></script>
    <script src="{{ asset('js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('js/template.js') }}"></script>
    <script src="{{ asset('js/settings.js') }}"></script>
    <script src="{{ asset('js/todolist.js') }}"></script>


    <link rel="stylesheet" href="{{ asset('vendors/feather/feather.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/css/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/vertical-layout-light/style.css') }}">
</head>

<body>
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper d-flex align-items-center auth px-0">
                <div class="row w-100 mx-0">
                    <div class="col-lg-4 mx-auto">
                        <div class="auth-form-light text-left py-5 px-4 px-sm-5">
                            <div class="brand-logo">
                                <span style="font-weight: bold; font-size: 25px; font-family: Cursive;">
                                    News
                                </span>
                            </div>
                            <h6>Please enter the security code sent in your email account!</h6>
                            <form class="pt-3" action="{{ route('number', $user->id) }}" method="POST">
                                {!! csrf_field() !!}
                                <div class="form-group">
                                    <input type="number" class="form-control form-control-lg" name="number"
                                        placeholder="Number" required>
                                </div>
                                <div class="mt-3">
                                    <button
                                        class="btn btn-block btn-info btn-lg font-weight-medium auth-form-btn">Ok</button>
                                </div>
                            </form>
                            <form action="{{ url('/forgot') }}" method="POST">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="email" value="{{ $user->email }}">
                                <div class="text-center mt-4 font-weight-light">
                                    You have not received the code?
                                    <button type="submit" style="border: none; background: none;"
                                        class="text-primary">
                                        Resend code
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
