<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="{{ asset('vendors/js/vendor.bundle.base.js') }}"></script>
    <script src="{{ asset('js/off-canvas.js') }}"></script>
    <script src="{{ asset('js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('js/template.js') }}"></script>
    <script src="{{ asset('js/settings.js') }}"></script>
    <script src="{{ asset('js/todolist.js') }}"></script>


    <link rel="stylesheet" href="{{ asset('vendors/feather/feather.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/css/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/vertical-layout-light/style.css') }}">


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .comment-text {
            font-size: 12px
        }

        .fs-10 {
            font-size: 14px
        }

        .card {
            position: relative;
            display: flex;
            padding: 20px;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
            border: 1px solid #d2d2dc;
            border-radius: 11px;
            -webkit-box-shadow: 0px 0px 5px 0px rgb(249, 249, 250);
            -moz-box-shadow: 0px 0px 5px 0px rgba(212, 182, 212, 1);
            box-shadow: 0px 0px 5px 0px rgb(161, 163, 164)
        }

        .reply a {
            text-decoration: none
        }

        .scrollbar {
            height: 350px;
            overflow-y: scroll;
        }

        #style-1::-webkit-scrollbar {
            width: 4px;
            background-color: white;
        }

        #style-1::-webkit-scrollbar-thumb {
            background-color: gray;
        }

        #style-1::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 4px rgba(0, 0, 0, 0.2);
            background-color: white;
        }

        .date {
            font-size: 14px
        }

        .comment-text {
            font-size: 17px
        }

        .fs-12 {
            font-size: 17px
        }

        .shadow-none {
            box-shadow: none
        }

        .name {
            color: #007bff
        }

        .cursor:hover {
            color: blue
        }

        .cursor {
            cursor: pointer
        }

        .textarea {
            resize: none
        }

        .fa {
            cursor: pointer
        }

        .img-278 {
            height: 360px;
            display: block;
            margin-left: 0;
        }

        .img-512 {
            height: 360px;
            display: block;
            margin-left: auto;
            margin-right: auto;
        }

    </style>

    <title>News</title>
</head>

<body>
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper">
            @include('layout.header')
            @yield('content')
        </div>
    </div>
    @include('layout.footer')
</body>
<script type="text/javascript">
    $(document).ready(function() {
        // create post
        $('#form-create').submit(function(e) {
            let url = $(this).attr('data-url');
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    title: $('#title-create').val(),
                    content: $('#content-create').val(),
                    user_id: $('#id_user').val()
                },
                success: function(response) {
                    window.location.reload();
                    alert("Successful post!");
                }
            });
        });
        // edit post
        $('.btnEdit').click(function() {
            let url = $(this).attr('data-url');
            $.ajax({
                url: url,
                method: "GET",
                success: function(response) {
                    console.log(response);
                    console.log(response.data.title);

                    $('#title-edit').val(response.data.title);
                    $('#content-edit').val(response.data.content);
                    $('#form-edit').attr('data-url', '/api/post-update/' + response.data
                        .id);
                }
            });
        });
        // update post
        $('#form-edit').submit(function() {
            let url = $(this).attr('data-url');
            $.ajax({
                url: url,
                method: "PUT",
                data: {
                    title: $('#title-edit').val(),
                    content: $('#content-edit').val()
                },
                success: function(response) {
                    window.location.reload();
                    alert("Successful edit!");
                }
            });
        });
        // delete post
        $('.btnDelete').click(function() {
            let url = $(this).attr('data-url');
            if (confirm('Are you sure to delete this post?')) {
                $.ajax({
                    url: url,
                    method: 'delete',
                    success: function(response) {
                        window.location.reload();
                    }
                });
            }
        });
        // edit comment
        $('.btnEditComment').click(function() {
            let url = $(this).attr('data-url');
            $.ajax({
                url: url,
                method: "GET",
                success: function(response) {
                    console.log(response);

                    $('#content-edit').val(response.data.content);
                    $('#form-editcomment').attr('action', '/comment-update/' +
                        response.data.id);
                }
            });
        });
        // update comment
        $('#form-editcomment').submit(function() {
            let url = $(this).attr('data-url');
            $.ajax({
                url: url,
                method: "PUT",
                data: {
                    content: $('#content-edit').val()
                }
            });
        });
        // edit reply comment
        $('.btnEditReplyComment').click(function() {
            let url = $(this).attr('data-url');
            $.ajax({
                url: url,
                method: "GET",
                success: function(response) {
                    console.log(response);

                    $('#reply_content-edit').val(response.data.content);
                    $('#form-editreplycomment').attr('action', '/reply-comment-update/' +
                        response.data.id);
                }
            });
        });
        // update reply comment
        $('#form-editreplycomment').submit(function() {
            let url = $(this).attr('data-url');
            $.ajax({
                url: url,
                method: "PUT",
                data: {
                    content: $('#reply_content-edit').val()
                }
            });
        });
        // ajax search
        $('#country_name').keyup(function() {
            var query = $(this).val();
            if (query != '') {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url: "{{ route('search') }}",
                    method: "POST",
                    data: {
                        query: query,
                        _token: _token
                    },
                    success: function(data) {
                        $('#countryList').fadeIn();
                        $('#countryList').html(
                            data
                        );
                    }
                });
            }
        });
        $(document).on('click', 'li', function() {
            $('#country_name').val($(this).text());
            $('#countryList').fadeOut();
        });
    });
</script>

</html>
