<footer class="footer">
    <div class="d-sm-flex justify-content-center justify-content-sm-between">
        <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">
            Copyright © gf-hun.com 2020
        </span>
        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">
            <h6>Welcome to the <a href="{{ url('/') }}">news</a> list!</h6>
        </span>
    </div>
</footer>
