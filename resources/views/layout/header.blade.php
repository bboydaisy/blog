<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
    <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo mr-5" href="{{ url('/') }}">
            <span style="font-weight: bold; font-size: 25px; font-family: Cursive;">
                <img src="{{ asset('img/logo-mini.svg') }}" alt="logo">
                News
            </span>
        </a>
        <a class="navbar-brand brand-logo-mini" href="{{ url('/') }}">
            <img src="{{ asset('img/logo-mini.svg') }}" alt="logo">
        </a>
    </div>
    <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span class="icon-menu"></span>
        </button>
        <ul class="navbar-nav mr-lg-2">
            <li class="nav-item nav-search d-none d-lg-block">
                <div class="input-group">
                    <div class="input-group-prepend hover-cursor" id="navbar-search-icon">
                        <span class="input-group-text" id="search">
                            <i class="icon-search"></i>
                        </span>
                    </div>
                    <input type="search" name="country_name" id="country_name" class="form-control rounded input-lg"
                        placeholder="Search news..." aria-label="search" aria-describedby="search">
                    <div id="countryList">
                    </div>
                    {{ csrf_field() }}
                </div>
            </li>
        </ul>
        <ul class="navbar-nav navbar-nav-right">
            @if (isset($posts))
                <li class="nav-item nav-profile dropdown">
                    <button type="button" class="btn btn-primary font-weight-bold" data-toggle="modal"
                        data-target="#modalCreatePost">
                        <i class="icon-upload"></i>
                        New post
                    </button>
                </li>
            @endif
            <li class="nav-item nav-profile dropdown">
                <a class="nav-link dropdown-toggle" type="button" data-toggle="dropdown" id="profileDropdown">
                    @if (Auth::user()->avatar !== null)
                        <img src="{{ Auth::user()->avatar }}" alt="profile">
                    @else
                        <img src="{{ asset('img/ho.jpg') }}" alt="profile">
                    @endif
                    <span style="font-weight: bold; font-family: Cursive;">{{ Auth::user()->name }}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                    <a class="dropdown-item" href="{{ url('/user') }}">
                        <i class="ti-settings text-primary"></i>
                        Profile
                    </a>
                    <a class="dropdown-item" href="{{ url('/your-post') }}">
                        <i class="mdi mdi-book-open-page-variant text-primary"></i>
                        Your post
                    </a>
                    <a class="dropdown-item" href="/statistical">
                        <i class="mdi mdi-book-open-page-variant text-primary"></i>
                        Statistical
                    </a>
                    <a class="dropdown-item" href="{{ url('/logout') }}">
                        <i class="ti-power-off text-primary"></i>
                        Logout
                    </a>
                </div>
            </li>
            <li class="nav-item nav-settings d-none d-lg-flex">
                <a class="nav-link" type="button">
                    <i class="icon-ellipsis"></i>
                </a>
            </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
            data-toggle="offcanvas">
            <span class="icon-menu"></span>
        </button>
    </div>
</nav>

<div class="theme-setting-wrapper">
    <div id="settings-trigger"><i class="ti-settings"></i></div>
    <div id="theme-settings" class="settings-panel">
        <i class="settings-close ti-close"></i>
        <p class="settings-heading">SIDEBAR SKINS</p>
        <div class="sidebar-bg-options selected" id="sidebar-light-theme">
            <div class="img-ss rounded-circle bg-light border mr-3"></div>Light
        </div>
        <div class="sidebar-bg-options" id="sidebar-dark-theme">
            <div class="img-ss rounded-circle bg-dark border mr-3"></div>Dark
        </div>
        <p class="settings-heading mt-2">HEADER SKINS</p>
        <div class="color-tiles mx-0 px-4">
            <div class="tiles success"></div>
            <div class="tiles warning"></div>
            <div class="tiles danger"></div>
            <div class="tiles info"></div>
            <div class="tiles dark"></div>
            <div class="tiles default"></div>
        </div>
    </div>
</div>
<div id="right-sidebar" class="settings-panel">
    <i class="settings-close ti-close"></i>
    <ul class="nav nav-tabs border-top" id="setting-panel" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="todo-tab" data-toggle="tab" role="tab" aria-controls="todo-section"
                aria-expanded="true">Notifications</a>
        </li>
    </ul>
    <div class="tab-content" id="setting-content">
        <!-- To do section tab ends -->
        <div class="tab-pane fade show active scroll-wrapper" id="todo-section" role="tabpanel"
            aria-labelledby="todo-section">
            <div class="d-flex align-items-center justify-content-between border-bottom">
                <p class="settings-heading border-top-0 mb-3 pl-3 pt-0 border-bottom-0 pb-0">Everyone</p>
                <small class="settings-heading border-top-0 mb-3 pt-0 border-bottom-0 pb-0 pr-3 font-weight-normal">See
                    All</small>
            </div>
            <ul class="chat-list">
                {{-- @foreach ($comments as $item)
                    <li class="list active">
                        <div class="profile">
                            @if ($item->user->avatar !== null)
                                <img src="{{ $item->user->avatar }}" alt="avatar">
                            @else
                                <img src="{{ asset('img/ho.jpg') }}" alt="avatar">
                            @endif
                            <span class="online"></span>
                        </div>
                        <div class="info">
                            <p>{{ $item->user->name }}</p>
                            <p>replied to your post</p>
                        </div>
                        <small class="text-muted my-auto">
                            {{ date('d/m/y', strtotime($item->created_at)) }},
                            {{ date('H:i', strtotime($item->created_at)) }}
                        </small>
                    </li>
                @endforeach --}}
            </ul>
        </div>
        <!-- chat tab ends -->
    </div>
</div>
