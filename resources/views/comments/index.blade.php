@extends('layout.main')

@section('content')
    <div class="container mb-5 mt-5">
        <div class="d-flex justify-content-center row">
            <div class="d-flex flex-column col-md-8">
                <div class="d-flex flex-column comment-section" id="myGroup">
                    <div class="bg-white p-2">
                        <div class="d-flex flex-row user-info">
                            @if ($post->user->avatar !== null)
                                <img class="rounded-circle" src="{{ $post->user->avatar }}" width="50">
                            @else
                                <img class="rounded-circle" src="{{ asset('img/ho.jpg') }}" width="50">
                            @endif
                            <div class="d-flex flex-column justify-content-start ml-2">
                                <span class="d-block font-weight-bold name">
                                    {{ $post->title }}
                                </span>
                                <span class="text-black-50">
                                    {{ $post->user->name }} -
                                    <span class="date text-black-50">
                                        {{ $post->time }}
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="mt-2">
                            <p class="comment-text">
                                {{ $post->content }}
                            </p>
                        </div>
                    </div>
                    <div class="bg-white p-2">
                        <div class="d-flex flex-row fs-12">
                            <div class="like p-2 cursor">
                                @if ($like == null)
                                    <form id="form-updatelike" action="{{ route('like', 0) }}" method="POST">
                                        @csrf
                                        <input id="user_id" type="hidden" name="user_id" value="{{ Auth::id() }}">
                                        <input id="post_id" type="hidden" name="post_id" value="{{ $post->id }}">
                                        <input id="confirm_flag" type="hidden" name="confirm_flag" value="1">
                                        <button style="background: none; border:none;" type="submit">
                                            <i class="fa fa-heart-o"></i>
                                            <span class="ml-1">{{ $post->total_like }}</span>
                                        </button>
                                    </form>
                                @else
                                    @if ($like->confirm_flag == 1)
                                        <form id="form-updatelike" action="{{ route('like', $like->id) }}" method="POST">
                                            @csrf
                                            <input id="user_id" type="hidden" name="user_id" value="{{ Auth::id() }}">
                                            <input id="post_id" type="hidden" name="post_id" value="{{ $post->id }}">
                                            <input id="confirm_flag" type="hidden" name="confirm_flag" value="0">
                                            <button style="background: none; border:none;" type="submit">
                                                <i class="fa fa-heart text-danger"></i>
                                                <span class="ml-1">{{ $post->total_like }}</span>
                                            </button>
                                        </form>
                                    @else
                                        <form id="form-updatelike" action="{{ route('like', $like->id) }}" method="POST">
                                            @csrf
                                            <input id="user_id" type="hidden" name="user_id" value="{{ Auth::id() }}">
                                            <input id="post_id" type="hidden" name="post_id" value="{{ $post->id }}">
                                            <input id="confirm_flag" type="hidden" name="confirm_flag" value="1">
                                            <button style="background: none; border:none;" type="submit">
                                                <i class="fa fa-heart-o"></i>
                                                <span class="ml-1">{{ $post->total_like }}</span>
                                            </button>
                                        </form>
                                    @endif
                                @endif
                            </div>
                            <div class="like p-2 cursor action-collapse" data-toggle="collapse" aria-expanded="true"
                                aria-controls="collapse-1" href="#collapse-1">
                                <i class="fa fa-comment-o"></i>
                                <span class="ml-1">Comment
                                    ({{ $comments->count() + $replycomments->count() }})</span>
                            </div>
                        </div>
                    </div>
                    <div id="collapse-1" class="bg-light p-2 collapse" data-parent="#myGroup">
                        <form action="{{ url('/comment-store') }}" method="POST">
                            @csrf
                            <div>
                                <input type="hidden" name="user_id" value="{{ Auth::id() }}">
                            </div>
                            <div>
                                <input type="hidden" name="post_id" value="{{ $post->id }}">
                            </div>
                            <div class="d-flex flex-row align-items-start">
                                @if (Auth::user()->avatar !== null)
                                    <img class="rounded-circle" src="{{ Auth::user()->avatar }}" width="40">
                                @else
                                    <img class="rounded-circle" src="{{ asset('img/ho.jpg') }}" width="40">
                                @endif
                                <textarea class="form-control ml-1 shadow-none textarea" name="content" required></textarea>
                            </div>
                            <div class="mt-2 text-right">
                                <button class="btn btn-primary shadow-none" type="submit">Post comment</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="container mb-5 mt-5">
                    <div class="card">
                        <div class="row">
                            <div class="col-md-12">
                                @if (Session::has('success'))
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        <strong>{{ Session::get('success') }}</strong>
                                        <a href="" class="close" data-dismiss="alert"
                                            aria-label="close">&times;</a>
                                    </div>
                                @endif
                                @if (Session::has('error'))
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <strong>{{ Session::get('error') }}</strong>
                                        <a href="" class="close" data-dismiss="alert"
                                            aria-label="close">&times;</a>
                                    </div>
                                @endif
                                @if ($errors->any())
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                        <a href="" class="close" data-dismiss="alert"
                                            aria-label="close">&times;</a>
                                    </div>
                                @endif
                                <h4 class="text-center mb-5"> Comment section </h4>
                                <div class="row">
                                    <div class="col-md-12">
                                        @if ($comments->count() == 0)
                                            <div class="media">
                                                <p class="m-b-5 m-t-10">
                                                    <span style="font-size: 16px;">There are currently no new comments,
                                                        please be the first.</span>
                                                </p>
                                            </div>
                                        @else
                                            @foreach ($comments as $item)
                                                <br>
                                                <div class="media">
                                                    @if ($item->user->avatar !== null)
                                                        <img class="mr-3 rounded-circle" src="{{ $item->user->avatar }}"
                                                            alt="user" width="48">
                                                    @else
                                                        <img class="mr-3 rounded-circle" src="{{ asset('img/ho.jpg') }}"
                                                            alt="user" width="48">
                                                    @endif
                                                    <div class="media-body">
                                                        <form action="{{ route('comment-delete', $item->id) }}"
                                                            method="POST">
                                                            @csrf
                                                            @method('DELETE')
                                                            <div class="row">
                                                                <div class="col-8 d-flex">
                                                                    <h6>{{ $item->user->name }}</h6>
                                                                    <span class="date text-black-50">
                                                                        -
                                                                        {{ $item->time }}
                                                                    </span>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="pull-right reply">
                                                                        @if ($item->check)
                                                                            <span class="action-icons">
                                                                                <a data-url="{{ route('comment-edit', $item->id) }}"
                                                                                    type="button"
                                                                                    class="text-primary btnEditComment"
                                                                                    data-toggle="modal"
                                                                                    data-target="#modalUpdateComment">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                                        width="16" height="16"
                                                                                        fill="currentColor"
                                                                                        class="bi bi-pencil-square"
                                                                                        viewBox="0 0 16 16">
                                                                                        <path
                                                                                            d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                                                                                        <path fill-rule="evenodd"
                                                                                            d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                                                                                    </svg>
                                                                                </a>
                                                                                <button
                                                                                    style="background: none; border:none;"
                                                                                    data-abc="true" type="submit"
                                                                                    onclick="return confirm('Are you sure to delete this comment?')">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                                        width="16" height="16"
                                                                                        fill="currentColor"
                                                                                        class="bi bi-trash text-danger"
                                                                                        viewBox="0 0 16 16">
                                                                                        <path
                                                                                            d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                                                                        <path fill-rule="evenodd"
                                                                                            d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                                                                                    </svg>
                                                                                </button>
                                                                            </span>
                                                                        @endif
                                                                        <a data-toggle="collapse" aria-expanded="true"
                                                                            aria-controls="collapse-2"
                                                                            href="#collapse-{{ $item->id }}">
                                                                            <span>
                                                                                <i class="fa fa-reply"></i>
                                                                                reply
                                                                            </span>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                        {{ $item->content }}
                                                        <div id="collapse-{{ $item->id }}"
                                                            class="bg-light p-2 collapse" data-parent="#myGroup">
                                                            <form action="{{ url('/reply-comment') }}" method="POST">
                                                                @csrf
                                                                <div>
                                                                    <input type="hidden" name="user_id"
                                                                        value="{{ Auth::id() }}">
                                                                </div>
                                                                <div>
                                                                    <input type="hidden" name="post_id"
                                                                        value="{{ $post->id }}">
                                                                </div>
                                                                <div>
                                                                    <input type="hidden" name="comment_id"
                                                                        value="{{ $item->id }}">
                                                                </div>
                                                                <div class="d-flex flex-row align-items-start">
                                                                    @if (Auth::user()->avatar !== null)
                                                                        <img class="rounded-circle"
                                                                            src="{{ Auth::user()->avatar }}" width="40">
                                                                    @else
                                                                        <img class="rounded-circle"
                                                                            src="{{ asset('img/ho.jpg') }}" width="40">
                                                                    @endif
                                                                    <textarea class="form-control ml-1 shadow-none textarea"
                                                                        name="content" required></textarea>
                                                                </div>
                                                                <div class="mt-2 text-right">
                                                                    <button class="btn btn-primary btn-sm shadow-none"
                                                                        type="submit">Post comment</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        @foreach ($replycomments as $data)
                                                            @if ($item->id == $data->comment_id)
                                                                <div class="media mt-4">
                                                                    <a class="pr-3" href="#">
                                                                        @if ($data->user->avatar !== null)
                                                                            <img class="rounded-circle"
                                                                                src="{{ $data->user->avatar }}"
                                                                                alt="user" width="48">
                                                                        @else
                                                                            <img class="rounded-circle"
                                                                                src="{{ asset('img/ho.jpg') }}"
                                                                                alt="user" width="48">
                                                                        @endif
                                                                    </a>
                                                                    <form
                                                                        action="{{ route('reply-comment-delete', $data->id) }}"
                                                                        method="POST">
                                                                        @csrf
                                                                        @method('DELETE')
                                                                        <div class="media-body">
                                                                            <div class="row">
                                                                                <div class="col-12 d-flex">
                                                                                    <h6>{{ $data->user->name }}</h6>
                                                                                    <span class="date text-black-50">
                                                                                        -
                                                                                        {{ $data->time }}
                                                                                    </span>
                                                                                    @if ($data->check)
                                                                                        <span style="color: white">-</span>
                                                                                        <span class="action-icons">
                                                                                            <a data-url="{{ route('reply-comment-edit', $data->id) }}"
                                                                                                type="button"
                                                                                                class="text-primary btnEditReplyComment"
                                                                                                data-toggle="modal"
                                                                                                data-target="#modalUpdateReplyComment">
                                                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                    width="16" height="16"
                                                                                                    fill="currentColor"
                                                                                                    class="bi bi-pencil-square"
                                                                                                    viewBox="0 0 16 16">
                                                                                                    <path
                                                                                                        d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                                                                                                    <path
                                                                                                        fill-rule="evenodd"
                                                                                                        d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                                                                                                </svg>
                                                                                            </a>
                                                                                            <button
                                                                                                style="background: none; border:none;"
                                                                                                data-abc="true"
                                                                                                type="submit"
                                                                                                onclick="return confirm('Are you sure to delete this comment?')">
                                                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                    width="16" height="16"
                                                                                                    fill="currentColor"
                                                                                                    class="bi bi-trash text-danger"
                                                                                                    viewBox="0 0 16 16">
                                                                                                    <path
                                                                                                        d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                                                                                    <path
                                                                                                        fill-rule="evenodd"
                                                                                                        d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                                                                                                </svg>
                                                                                            </button>
                                                                                        </span>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                            <span style="font-weight: 600">
                                                                                {{ $data->comment->user->name }}@
                                                                            </span>
                                                                            {{ $data->content }}
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- modalUpdate Comment --}}
        <div class="modal fade" id="modalUpdateComment" tabindex="-1" aria-labelledby="EditCommentModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="" id="form-editcomment" method="post" style="overflow-y: auto">
                        @csrf
                        @method('PUT')
                        <div class="modal-header">
                            <h4>Edit comment</h4>
                        </div>
                        <div class="modal-body">
                            <label for="content-edit" class="col-form-control">
                                <b>Content:</b>
                            </label>
                            <textarea class="form-control" style="height:100px; width:100%" placeholder="Add content..."
                                name="content" id="content-edit" required></textarea>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <div>
                                <button type="submit" class="btn btn-primary" id="submit">Edit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{-- modalUpdate ReplyComment --}}
        <div class="modal fade" id="modalUpdateReplyComment" tabindex="-1" aria-labelledby="EditreplyCommentModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="" id="form-editreplycomment" method="post" style="overflow-y: auto">
                        @csrf
                        @method('PUT')
                        <div class="modal-header">
                            <h4>Edit comment</h4>
                        </div>
                        <div class="modal-body">
                            <label for="reply_content-edit" class="col-form-control">
                                <b>Content:</b>
                            </label>
                            <textarea class="form-control" style="height:100px; width:100%" placeholder="Add content..."
                                name="content" id="reply_content-edit" required></textarea>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <div>
                                <button type="submit" class="btn btn-primary" id="submit">Edit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
