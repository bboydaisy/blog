@extends('layout.main')

@section('content')
    <div class="container mt-3 justify-content-center">
        <div class="row d-flex justify-content-center">
            <div class="col-md-8">
                <div class="text-left">
                </div>
                @foreach ($posts as $item)
                    <a href="{{ route('comment', $item->id) }}" style="text-decoration: none; color: black;">
                        <div class="card p-3 mb-2">
                            <div class="d-flex flex-row">
                                @if ($item->user->avatar !== null)
                                    <img src="{{ $item->user->avatar }}" height="40" width="40" class="rounded-circle">
                                @else
                                    <img src="{{ asset('img/ho.jpg') }}" height="40" width="40" class="rounded-circle">
                                @endif
                                <div class="d-flex flex-column ms-2">
                                    <h6 class="mb-1 text-primary">{{ $item->title }}</h6>
                                    <p><span class="text-black-50" style="font-size: 16px">{{ $item->user->name }}</span>
                                    </p>
                                    <p class="comment-text">{{ $item->content }}</p>
                                </div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="d-flex flex-row gap-3 align-items-center">
                                </div>
                                <div class="d-flex flex-row">
                                    <span class="text-muted fw-normal">
                                        {{ $item->time }}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </a>
                @endforeach
                <div div="row">
                    <div class="pagination justify-content-end">
                        {{ $posts->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- modalCreate Post --}}
    <div class="modal fade" id="modalCreatePost" tabindex="-1" aria-labelledby="CreatePostModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="form-create" data-url="{{ route('post-store') }}" method="POST" style="overflow-y: auto">
                    @csrf
                    <div class="modal-header">
                        <h4>New post</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <div>
                                <input type="hidden" value="{{ Auth::id() }}" id="id_user" name="user_id">
                            </div>
                            <div>
                                <label for="title-create" class="col-form-label"><b>Title: <span
                                            style="color: red">*</span></b></label>
                                <input type="text" class="form-control" name="title" id="title-create"
                                    placeholder="Add title..." required>
                            </div>
                            <div>
                                <label for="title-create" class="col-form-label"><b>Content: <span
                                            style="color: red">*</span></b></label>
                                <textarea class="form-control" style="height:100px" placeholder="Add content..."
                                    name="content" id="content-create" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <div>
                            <button type="submit" class="btn btn-primary">Post</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
