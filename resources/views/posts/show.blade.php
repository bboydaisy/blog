@extends('layout.main')

@section('content')
    <div class="container mt-3 justify-content-center">
        <div class="row d-flex justify-content-center">
            <div class="col-md-8">
                <div class="text-twitter">
                    <h6>All posts({{ $post->count() }})</h6>
                </div>
                @foreach ($post as $item)
                    <a href="{{ route('comment', $item->id) }}" style="text-decoration: none; color: black;">
                        <div class="card p-3 mb-2">
                            <div class="d-flex flex-row">
                                @if ($item->user->avatar !== null)
                                    <img src="{{ $item->user->avatar }}" height="40" width="40" class="rounded-circle">
                                @else
                                    <img src="{{ asset('img/ho.jpg') }}" height="40" width="40" class="rounded-circle">
                                @endif
                                <div class="d-flex flex-column ms-2">
                                    <h6 class="mb-1 text-primary">{{ $item->title }}</h6>
                                    <p>
                                        <span class="text-black-50" style="font-size: 16px">
                                            {{ $item->user->name }} -
                                            <span class="date text-black-50">
                                                {{ $item->time }}
                                            </span>
                                        </span>
                                    </p>
                                    <p class="comment-text">{{ $item->content }}</p>
                                </div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="d-flex flex-row gap-3 align-items-center">

                                </div>
                                <div class="d-flex flex-row">
                                    <a class="nav-link count-indicator" id="notificationDropdown" href="#"
                                        data-toggle="dropdown">
                                        <i class="icon-ellipsis"></i>
                                        <span class="count"></span>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list"
                                        aria-labelledby="notificationDropdown">
                                        <a class="dropdown-item preview-item btnEdit"
                                            data-url="{{ route('post-edit', $item->id) }}" type="button"
                                            data-toggle="modal" data-target="#modalUpdatePost">
                                            <div class="preview">
                                                <div class="preview-icon bg-success">
                                                </div>
                                            </div>
                                            <div class="preview-item-content">
                                                <p class="font-weight-normal small-text mb-0 text-muted">
                                                    Edit post
                                                </p>
                                            </div>
                                        </a>
                                        <a class="dropdown-item preview-item btnDelete"
                                            data-url="{{ route('post-delete', $item->id) }}" type="submit">
                                            <div class="preview">
                                                <div class="preview-icon bg-warning">
                                                </div>
                                            </div>
                                            <div class="preview-item-content">
                                                <p class="font-weight-normal small-text mb-0 text-muted">
                                                    Delete post
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                @endforeach
                <div div="row">
                    <div class="pagination justify-content-end">
                        {{ $post->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- modalUpdate Post --}}
    <div class="modal fade" id="modalUpdatePost" tabindex="-1" aria-labelledby="EditPostModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form dta-url="" id="form-edit" method="post" style="overflow-y: auto">
                    <div class="modal-header">
                        <h4>Edit post</h4>
                    </div>
                    <div class="modal-body">
                        <label for="title-edit" class="col-form-control">
                            <b>Title:</b>
                            <input type="text" size="100%" class="form-control" name="title" id="title-edit"
                                placeholder="Add title..." required>
                        </label>

                        <label for="content-edit" class="col-form-control">
                            <b>Content:</b>
                        </label>
                        <textarea class="form-control" style="height:100px; width:100%" placeholder="Add content..."
                            name="content" id="content-edit" required></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <div>
                            <button type="submit" class="btn btn-primary" id="submit">Edit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
