<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
    <div style="text-align: center; box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.4);">
        <p style="text-align: center;">
            <span style="font-size: 24px;">welcome to my website, {{ $email_data['name'] }}</span>
        </p>
        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSS150td34_IoLvT6U8hj-QPcUVUjHnOCj4qDDrnauJVR8U4Uqjtm2XLavcLOQb8eXGOI0&usqp=CAU"
            alt="" style="width: 150px; display: block; margin-left: auto; margin-right: auto;">
        <p style="text-align: center;">
            <span style="font-size: 16px;">
                Please click the below link to verify your email and activate your
                account!
            </span>
            <br><br><br>
            <a href="http://127.0.0.1:8000/verify?code={{ $email_data['verification_code'] }}"
                style="background: #008B00; border-radius: 10px 10px; padding: 12px 25px; text-decoration: none; color: white;">Accept</a>
        </p>
    </div>
</body>

</html>
