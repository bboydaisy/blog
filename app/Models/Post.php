<?php

namespace App\Models;

use App\Models\comment;
use App\Models\User;
use App\Models\Like;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'content',
        'total_like',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comment()
    {
        return $this->hasMany(Comment::class);
    }

    public function like()
    {
        return $this->hasMany(Like::class);
    }

    public function getTimeAttribute()
    {
        $time = Carbon::create($this->created_at)->diffForHumans(Carbon::now());
        return $time;
    }
}
