<?php

namespace App\Models;

use App\Models\Post;
use App\Models\User;
use App\Models\ReplyComment;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = [
        'content',
        'user_id',
        'post_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function reply_comment()
    {
        return $this->hasMany(ReplyComment::class);
    }

    public function getTimeAttribute()
    {
        $time = Carbon::create($this->created_at)->diffForHumans(Carbon::now());
        return $time;
    }
}
