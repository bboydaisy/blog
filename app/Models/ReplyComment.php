<?php

namespace App\Models;

use App\Models\Comment;
use App\Models\User;
use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReplyComment extends Model
{
    use HasFactory;

    protected $fillable = [
        'content',
        'user_id',
        'comment_id',
        'post_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comment()
    {
        return $this->belongsTo(Comment::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function getTimeAttribute()
    {
        $time = Carbon::create($this->created_at)->diffForHumans(Carbon::now());
        return $time;
    }
}
