<?php

namespace App\Http\Controllers;

use App\Models\Like;
use App\Models\Post;
use Illuminate\Http\Request;

class LikeController extends Controller
{
    public function store(Request $request, $id)
    {
        $like = Like::find($id);
        $post = Post::find($request->post_id);
        if ($id > 0) {
            if ($request->confirm_flag == 1) {
                $like->update(['confirm_flag' => 1]);
                $post->total_like++;
                $post->update();
                return redirect()->back();
            }
            $like->update(['confirm_flag' => 0]);
            $post->total_like--;
            $post->update();
            return redirect()->back();
        }
        Like::create($request->all());
        $post->total_like++;
        $post->save();
        return redirect()->back();
    }
}
