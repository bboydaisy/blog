<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;

class PostApiController extends Controller
{

    public function store(Request $request)
    {
        $post = Post::create($request->all());
        return response()->json(['data' => $post], 200);
    }

    public function show($id)
    {
        $post = Post::find($id)->load('user');
        return response()->json(['data' => $post], 200);
    }

    public function edit($id)
    {
        $post = Post::find($id);
        return response()->json(['data' => $post], 200);
    }

    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        $post->update($request->all());
        return response()->json(['data' => $post], 200);
    }

    public function destroy($id)
    {
        // dd($id);
        $post = Post::find($id);
        $post->comment()->delete();
        $post->delete();
    }
}
