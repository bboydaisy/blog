<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthenticateApiController extends Controller
{
    public static function loginToken()
    {
        $user = Auth::user();
        $token = $user->createToken('mytoken')->plainTextToken;
        $response = [
            'access_token' => $token,
            'token_type' => 'Bearer',
        ];
        return response()->json($response);
    }

    public static function logoutToken()
    {
        $user = Auth::user();
        $user->tokens()->delete();
    }
}
