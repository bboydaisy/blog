<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Like;
use App\Models\Post;
use App\Models\ReplyComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{

    public function show($id)
    {
        $post = Post::find($id);
        $like = Like::where('post_id', $id)->where('user_id', Auth::id())->first();
        $comments = Comment::where('post_id', $post->id)->orderBy('id', 'desc')->get();
        $comments->map(function ($item) {
            if ($item->user_id == Auth::id()) {
                $item->check = true;
            } else {
                $item->check = false;
            }
        });
        $replycomments = ReplyComment::where('post_id', $post->id)->get();
        $replycomments->map(function ($data) {
            if ($data->user_id == Auth::id()) {
                $data->check = true;
            } else {
                $data->check = false;
            }
        });
        return view('comments.index', compact('post', 'like',  'comments', 'replycomments'));
    }

    public function store(Request $request)
    {
        Comment::create($request->all());
        return redirect()->back()->with('success', 'Add comment successfully!');
    }

    public function edit($id)
    {
        $comment = Comment::find($id);
        if ($comment->user_id == Auth::id()) {
            return response()->json(['data' => $comment], 200);
        }
        return redirect()->back()->with('error', 'Insufficient access authority');
    }

    public function update(Request $request, $id)
    {
        $comment = Comment::find($id);
        if ($comment->user_id == Auth::id()) {
            $comment->content = $request->content;
            $comment->update();
            return redirect()->back()->with('success', 'Edit comment successfully!');
        }
        return redirect()->back()->with('error', 'Insufficient access authority');
    }

    public function destroy($id)
    {
        $comment = Comment::find($id);
        if ($comment->user_id == Auth::id()) {
            $comment->delete();
            return redirect()->back()->with('success', 'Delete comment successfully!');
        }
        return redirect()->back()->with('error', 'Insufficient access authority');
    }
}
