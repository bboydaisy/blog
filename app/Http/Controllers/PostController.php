<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use App\Models\ReplyComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::orderBy('id', 'desc')->paginate(20)->withQueryString();
        return view('posts.index', compact('posts'));
    }

    public function show()
    {
        $post = Post::where('user_id', Auth::id())->orderBy('id', 'desc')->paginate(20)->withQueryString();
        return view('posts.show', compact('post'));
    }

    function getSearchAjax(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            $data = Post::where('title', 'LIKE', "%{$query}%")
                ->orWhere('content', 'LIKE', "%{$query}%")->get();
            $output = '<ul id="style-1" class="list-group" style="position:absolute; width: 500px;
            height: 600px; overflow-x: hidden; overflow-y: scroll;">';
            foreach ($data as $row) {
                $output .= '
                <li class="list-group-item">
                    <a style="font-size: 16px; color: black; font-weight: 400; text-decoration: none;" href="/comment/' . $row->id . '">
                    <span class="font-weight-normal">' . $row->title . '</span><br>
                    <span class="font-weight-normal text-muted text-sm-left">' . $row->content . '</span>
               </a>
               </li>
               ';
            }
            $output .= '</ul>';
            echo $output;
        }
    }
}
