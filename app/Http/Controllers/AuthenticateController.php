<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\AuthenticateApiController;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class AuthenticateController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }

    public function login(LoginRequest $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'confirm_flag' => 1])) {
            AuthenticateApiController::loginToken();
            return redirect('/');
        }
        return redirect('/login')->with('error', 'Incorrect or unverified account!');
    }

    public function register()
    {
        return view('auth.register');
    }

    public function signup(RegisterRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->verification_code = sha1(time());
        $user->save();
        EmailController::SignupEmail($user->name, $user->email, $user->verification_code);
        return redirect('/register')->with('success', 'Your account has been created. Please check email verification link!');
    }

    public function verify(Request $request)
    {
        if ($user = User::where('verification_code', $request->code)->first()) {
            $user->verification_code = null;
            $user->confirm_flag = 1;
            $user->update();
            Auth::login($user);
            AuthenticateApiController::loginToken();
            return redirect('/');
        }
        return redirect('/login')->with('error', 'Invalid verification code!');
    }

    public function logout()
    {
        AuthenticateApiController::logoutToken();
        Auth::logout();
        return redirect('/login')->with('success', 'Successful logout!');
    }

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback()
    {
        $google = Socialite::driver('google')->user();

        if (!$user = User::where('provider_id', $google->id)->first()) {
            try {
                $user = new User();
                $user->name = $google->name;
                $user->email = $google->email;
                $user->provider_id = $google->id;
                $user->avatar = $google->avatar;
                $user->save();
            } catch (\Throwable) {
                return redirect('/login')->with('error', 'Sorry, the account is already register!');
            }
        }
        Auth::login($user);
        AuthenticateApiController::loginToken();
        return redirect('/');
    }
}
