<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function edit()
    {
        $user = Auth::user();
        return view('user.edit', compact('user'));
    }

    public function update(UserRequest $request)
    {
        $user = Auth::user();

        if ($request->password) {
            if (!Hash::check($request->old_password, $user->password)) {
                return redirect()->back()->with('error', 'Your password is not correct!');
            }
            $user->password = Hash::make($request->password);
        }
        $user->name = $request->name;
        $user->update();
        return redirect()->back()->with('success', 'Successfully updated!');
    }

    public function destroy(Request $request)
    {
        $user = Auth::user();

        if ($request->password) {
            if (!Hash::check($request->password, $user->password)) {
                return redirect()->back()->with('error', 'Your password is not correct!');
            }
        }
        $user->comment()->delete();
        $user->post()->delete();
        $user->delete();
        return redirect('/login')->with('success', 'Your account has been disabled!');
    }

    public function forgot(Request $request)
    {
        try {
            if ($user = User::where('email', $request->email)->where('confirm_flag', 1)->first()) {
                $user->verification_code = random_int(100000, 999999);
                $user->update();
                SendEmailController::SendEmail($user->name, $user->email, $user->verification_code);
                return view('user.number', compact('user'));
            }
            return redirect('/login')->with('error', 'Sorry, email is not valid to access!');
        } catch (\Throwable) {
            return redirect('/login');
        }
    }

    public function number(Request $request, $id)
    {
        try {
            if ($user = User::where('id', $id)->where('verification_code', $request->number)->first()) {
                if ($user->time < 4) {
                    return view('user.password', compact('user'));
                }
                return redirect('/login')->with('error', 'Your code has expired!');
            }
            return redirect('/login')->with('error', 'Sorry your code is not correct!');
        } catch (\Throwable) {
            return redirect('/login');
        }
    }

    public function password(Request $request, $id)
    {
        try {
            $user = User::find($id);
            $user->verification_code = null;
            $user->password = Hash::make($request->password);
            $user->update();
            return redirect('/login')->with('success', 'Update successful, please login!');
        } catch (\Throwable) {
            return redirect('/login');
        }
    }
}
