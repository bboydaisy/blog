<?php

namespace App\Http\Controllers;

use App\Models\ReplyComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReplyCommentController extends Controller
{
    public function store(Request $request)
    {
        ReplyComment::create($request->all());
        return redirect()->back()->with('success', 'Reply comment successfully!');
    }

    public function edit($id)
    {
        $reply_comment = ReplyComment::find($id);
        if ($reply_comment->user_id == Auth::id()) {
            return response()->json(['data' => $reply_comment], 200);
        }
        return redirect()->back()->with('error', 'Insufficient access authority');
    }

    public function update(Request $request, $id)
    {
        $reply_comment = ReplyComment::find($id);
        if ($reply_comment->user_id == Auth::id()) {
            $reply_comment->content = $request->content;
            $reply_comment->update();
            return redirect()->back()->with('success', 'Edit comment successfully!');
        }
        return redirect()->back()->with('error', 'Insufficient access authority');
    }

    public function destroy($id)
    {
        $reply_comment = ReplyComment::find($id);
        if ($reply_comment->user_id == Auth::id()) {
            $reply_comment->delete();
            return redirect()->back()->with('success', 'Delete comment successfully!');
        }
        return redirect()->back()->with('error', 'Insufficient access authority');
    }
}
