<?php

use App\Http\Controllers\AuthenticateController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\LikeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ReplyCommentController;
use App\Http\Controllers\StatisticalController;
use App\Http\Controllers\UserController;
use App\Http\Middleware\CheckLogin;
use App\Http\Middleware\CheckLogout;
use Illuminate\Support\Facades\Route;

Route::middleware([CheckLogin::class])->group(function () {
    // post
    Route::get('/', [PostController::class, 'index']);
    Route::get('/your-post', [PostController::class, 'show']);
    // search
    Route::post('search/name', [PostController::class, 'getSearchAjax'])->name('search');
    // comment
    Route::get('/comment/{id}', [CommentController::class, 'show'])->name('comment');
    Route::post('/comment-store', [CommentController::class, 'store']);
    Route::get('/comment-edit/{id}', [CommentController::class, 'edit'])->name('comment-edit');
    Route::put('/comment-update/{id}', [CommentController::class, 'update'])->name('comment-update');
    Route::delete('/comment-delete/{id}', [CommentController::class, 'destroy'])->name('comment-delete');
    // reply comment
    Route::post('/reply-comment', [ReplyCommentController::class, 'store']);
    Route::get('/reply-comment-edit/{id}', [ReplyCommentController::class, 'edit'])->name('reply-comment-edit');
    Route::put('/reply-comment-update/{id}', [ReplyCommentController::class, 'update'])->name('reply-comment-update');
    Route::delete('/reply-comment-delete/{id}', [ReplyCommentController::class, 'destroy'])->name('reply-comment-delete');
    // like
    Route::post('/like/{id}', [LikeController::class, 'store'])->name('like');
    //statistical
    Route::get('/statistical', [StatisticalController::class, 'index']);
    // user
    Route::get('/user', [UserController::class, 'edit']);
    Route::put('/user', [UserController::class, 'update']);
    Route::delete('/user', [UserController::class, 'destroy']);
    // logout
    Route::get('/logout', [AuthenticateController::class, 'logout']);
});

Route::middleware([CheckLogout::class])->group(function () {
    // login
    Route::get('/login', [AuthenticateController::class, 'index']);
    Route::post('/login', [AuthenticateController::class, 'login']);
    // Google login
    Route::get('/login/google', [AuthenticateController::class, 'redirectToGoogle']);
    Route::get('/callback/google', [AuthenticateController::class, 'handleGoogleCallback']);
    // register
    Route::get('/register', [AuthenticateController::class, 'register']);
    Route::post('/register', [AuthenticateController::class, 'signup']);
    // email
    Route::get('verify', [AuthenticateController::class, 'verify']);
});
// forgot password
Route::put('/forgot', [UserController::class, 'forgot']);
Route::post('/number/{id}', [UserController::class, 'number'])->name('number');
Route::put('/password/{id}', [UserController::class, 'password'])->name('password');
