<?php

use App\Http\Controllers\Api\PostApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/post-store', [PostApiController::class, 'store'])->name('post-store');
Route::get('/post-show/{id}', [PostApiController::class, 'show'])->name('post-show');
Route::get('/post-edit/{id}', [PostApiController::class, 'edit'])->name('post-edit');
Route::put('/post-update/{id}', [PostApiController::class, 'update'])->name('post-update');
Route::delete('/post-delete/{id}', [PostApiController::class, 'destroy'])->name('post-delete');
